// Task list array to store tasks
let taskList = [];

// Function to render the task list
function renderTaskList() {
  const taskListElement = document.getElementById("taskList");
  taskListElement.innerHTML = "";

  taskList.forEach((task, index) => {
    const taskItem = document.createElement("div");
    taskItem.classList.add(
      "flex",
      "items-center",
      "bg-white",
      "shadow",
      "rounded",
      "py-2",
      "px-4",
      "mb-4"
    );

    const taskText = document.createElement("span");
    taskText.classList.add("flex-1", "text-gray-800");
    taskText.textContent = task;

    const taskActions = document.createElement("div");
    taskActions.classList.add("ml-4");

    const completeButton = document.createElement("button");
    completeButton.classList.add(
      "bg-green-500",
      "text-white",
      "py-1",
      "px-2",
      "rounded",
      "hover:bg-green-600",
      "focus:outline-none",
      "focus:ring-2",
      "focus:ring-green-500"
    );
    completeButton.innerHTML = '<i class="fas fa-check"></i>';
    completeButton.addEventListener("click", () => {
      completeTask(index);
    });

    const deleteButton = document.createElement("button");
    deleteButton.classList.add(
      "bg-red-500",
      "text-white",
      "py-1",
      "px-2",
      "rounded",
      "hover:bg-red-600",
      "focus:outline-none",
      "focus:ring-2",
      "focus:ring-red-500"
    );
    deleteButton.innerHTML = '<i class="fas fa-trash"></i>';
    deleteButton.addEventListener("click", () => {
      deleteTask(index);
    });

    taskActions.appendChild(completeButton);
    taskActions.appendChild(deleteButton);

    taskItem.appendChild(taskText);
    taskItem.appendChild(taskActions);

    taskListElement.appendChild(taskItem);
  });
}

// Function to add a new task
function addTask(event) {
  event.preventDefault();
  const taskInput = document.getElementById("taskInput");
  const task = taskInput.value.trim();

  if (task !== "") {
    taskList.push(task);
    taskInput.value = "";
    renderTaskList();
  }
}

// Function to mark a task as completed
function completeTask(index) {
  taskList.splice(index, 1);
  renderTaskList();
}

// Function to delete a task
function deleteTask(index) {
  taskList.splice(index, 1);
  renderTaskList();
}

// Event listener for form submission
const addTaskForm = document.getElementById("addTaskForm");
addTaskForm.addEventListener("submit", addTask);

// Initial rendering of the task list
renderTaskList();
